* To import projects into db:

```
cd node-web-server
mongoimport --host=127.0.0.1 -d test -c projects --jsonArray --file www_static/allProjects.json --upsert --upsertFields id 
```

* To import stations into db:

```
cd node-web-server
mongoimport --host=127.0.0.1 -d test -c stations --jsonArray --file www_static/TransMap/stations.json --upsert --upsertFields crsCode 
```

```
cd node-web-server
mongoimport --host=127.0.0.1 -d test -c liaisons --jsonArray --file www_static/TransMap/stations.json --upsert --upsertFields crsCode 
```