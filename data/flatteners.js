
exports.projects = function(d) {
	return {
		id: d.id,
		title: d.title,
		label: d.label,
		folder: d.folder,
		capsuleDesc: d.capsuleDesc,
		content: d.content,
		homeContent: d.homeContent,
		columns: d.columns,
		sortBy: d.sortBy,
		doubleWidth: d.doubleWidth,
		showSkills: d.showSkills,
		datesFrom: d.dates ? d.dates[0] : null,
		datesTo: d.dates ? d.dates[1] : null,
		thumb_yOffset: d.thumb ? d.thumb.yOffset : null,
		thumb_xOffset: d.thumb ? d.thumb.xOffset : null,
		thumb_path: d.thumb ? d.thumb.path : null,
		thumb_zoomFactor: d.thumb ? d.thumb.zoomFactor : null
	}
}

exports.skills = function(d) {
	return {
		id: d.skill,
		label: d.label,
		type: d.type,
		current: d.current,
		headline: d.headline,
		extUrl: d.extUrl
	}
}

exports.tags = function(d) {
	return {
		id: d.tag,
		label: d.label,
		colour: d.colour,
		posterProject: d.posterProject
	}
}

exports.stations = function(d) {
	return {
		crsCode: d.crsCode,
		name: d.name,
		coords_lat: d.coords[0],
		coords_long: d.coords[1]
	}
}
