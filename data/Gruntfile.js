module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON("package.json"),

		watch: {
			projects: {
				files: ['projects.json', 'projects.csv'],
				tasks: ['run:updateProjects']
			},
			skills: {
				files: ['skills.json'],
				tasks: ['run:updateSkills']
			},
			tags: {
				files: ['tags.json'],
				tasks: ['run:updateTags']
			}
		},
		
		run: {
			updateProjects: {
				cmd: "./upload_projects"
			},
			updateSkills: {
				cmd: "./upload_skills"
			},
			updateTags: {
				cmd: "./upload_tags"
			}
		}
	});

	grunt.event.on('watch', function(action, filepath, target) {
		grunt.log.writeln(target + ': ' + filepath + ' has ' + action);
	  });

	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-run');

	grunt.registerTask('default', ['watch']);
};