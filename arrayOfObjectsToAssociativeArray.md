
Given an array of objects `data`, such as that returned by a MongoDB query, you can transform this into an associative array (i.e. a JavaScript Object) like so:

```
Object.assign(...array.map(d => ({[d.skill]: d})))
```
---

1. For an object from the array `d`, the arrow function:
```
d => ({[d.skill]: d})
```
...returns a object with a key property (`d.skill` in this case) as the key and the full original object as the value.

The outer parentheses around the result are required because otherwise the { } would be interpreted as a function body declaration and not an object declaration (see [this](https://stackoverflow.com/questions/28770415/ecmascript6-arrow-function-that-returns-an-object))

The square brackets around `d.skill` are an example of [computed property names syntax](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Object_initializer#Computed_property_names).

2. array.map
```
array.map(d => ({[d.skill]: d}))
```
...returns an array of *objects*, each containing one key-value pair.

3. `Object.assign` takes a list of objects as parameters, assigning the properties of the second to the first, and so on, to return one 'merged' object. See [this](http://es6-features.org/#ObjectPropertyAssignment).

4. The [spread operator] `...` takes each element of an array and passes each one as a parameter to the function.