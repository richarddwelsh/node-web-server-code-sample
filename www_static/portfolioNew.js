var masonryTransition = '0.5s';

// Masonry library: https://masonry.desandro.com

// current status
var currentProjects = new Array();

var initialViewportHeight;

// static data objects
var skillMap, tagMap, projectData;

$.getJSON("/portfolio/skillMap", function(data){ 
	skillMap = data;
	if (window.initialSkill)
		currentProjects = skillMap[initialSkill].projects;
});
$.getJSON("/portfolio/tagMap", function(data){ 
	tagMap = data;
	if (window.initialTag)
		currentProjects = tagMap[initialTag].projects;
});
$.getJSON("/portfolio/projectData", function(data){ 
	projectData = data;
});

// PhotoSwipe
var pswpElement, pswpGallery, pswp;

// jQuery selections
var projects, projectCollections, viewport;
var masonryObj;

var mainMasonryOptions = 
	{
		itemSelector: ".sub-group:not(.unstaged), *:not(.sub-group) > a.project-tile:not(.unstaged)", // selects sub-groups and all projects not in a sub-group
		fitWidth: true,
		horizontalOrder: false,
		transitionDuration: masonryTransition,
		columnWidth: ".singleWidth"
	},
	subgroupMasonryOptions = 
	{
		itemSelector: ".project-tile:not(.unstaged)",
		transitionDuration: masonryTransition				
	};

$(document).ready(function(){
	projects = $(".project-tile");
	// tagSwitchers = $(".tagSwitcher");
	// commentsLinks = $(".commentsLink");
	// canvas = $("#tiled-area");
	projectCollections = $("#tiled-area");
	viewport = $("#viewport");

	$(window).on("resize", function() {
		scaleProjectArticle($(".project-article .scaleThis"));
	});

	$(window).on("orientationchange", function() {
		initialViewportHeight = undefined; // this will reset the initial viewport height, allowing scaled parts to grow vertically if necessary
	})

	// trick to ensure load event fires in IE on cached images
	$(".visual img").each(function() {
		$(this).attr("src", $(this).attr("src"))
	});
	
	// viewport.width($(window).innerWidth());
	
	$(".visual img").on("load", function () {
		// image loaded; flag as such and fire parent .visual load event
		log("IMG load: " + $(this).attr("src"))
		$(this).addClass("loaded");
		$(this).closest(".visual").trigger("load")
	})
	
	$(".project-article .visual").on("load", function() {
		log("article .visual load");
		
		var t = $(this);
		
		if (t.hasClass("loaded"))
		{
			if (t.hasClass("scaleThis"))
				scaleProjectArticle(t)
			else
				scaleProjectArticle(t.find(".scaleThis"));
		}
		else
		{
			if (t.find("img:not(.loaded)").length == 0) // all images have loaded
			{
				t.addClass("loaded");
				t.css({visibility: "visible"});
				scaleProjectArticle(t.find(".scaleThis"));
				// canvas.trigger("load");
				
			}
		}
	})

	$("#tiled-area .visual").on("load", function() {
		log("tile .visual load");
		
		var t = $(this);
		
		if (t.find("img:not(.loaded)").length == 0) // all images have loaded
		{
			t.addClass("loaded");
			t.css({visibility: "visible"});
			scaleProjectTile(t.find(".scaleThis"));
			t.closest("#tiled-area").trigger("load");
		}
	})
	
	// when the whole canvas has loaded, do the masonry
	projectCollections.on("load", function() {
		
		var t = $(this);

		if (!t.hasClass("loaded"))
		{
			if (t.find(".visual:not(.loaded)").length == 0) // all projects have loaded
			{
				log("all projects in collection have loaded");
				t.addClass("loaded");

				if (t.find(".singleWidth").length == 0)
					//debugMessage("*** only DW")

				// first layout any projects in sub-group divs
				t.find(".sub-group").masonry(subgroupMasonryOptions)

				mainMasonryOptions.columnWidth = (t.find(".singleWidth").length == 0) ?
					".doubleWidth" : ".singleWidth";
				
				// then layout the whole canvas, selecting the sub-groups and projects NOT in a sub-group
				masonryObj = t.masonry(mainMasonryOptions)
			}
		}
	})

	if ($(".cycler")) {
		setInterval('cycleImages()', 5000);
	}

	if (window.initialSkill)
		window.history.replaceState({type: "skill", id: initialSkill}, document.title, location.pathname);
	if (window.initialTag)
		window.history.replaceState({type: "tag", id: initialTag}, document.title, location.pathname);
	
	window.onpopstate = function(event) {
		if (event.state)
			switchSelection(event.state.type, event.state.id);
	}

	bindHandlers($(document));

	pswpElement	= $('.pswp')[0];

	pswpGallery = new Array();
	$("img.zoomable").each(function(n, o){
		var url = $(o).attr('src'),
			zoomUrl = $(o).attr('data-zoom-src') || url,
			zoomWidth = Number($(o).attr('data-zoom-width')),
			zoomHeight = Number($(o).attr('data-zoom-height'));

		pswpGallery.push({src: url, msrc: zoomUrl, w: zoomWidth, h: zoomHeight});
		$(o).attr("data-zoom-id", n).on("click", pswpClick)
	})
})

function pswpClick(event) {
	var index = Number($(this).attr("data-zoom-id"));
	pswp = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, pswpGallery, {
		shareEl: false,
		index: index,
		getThumbBoundsFn: function(index) {
			var thumbnail = $("img[data-zoom-id='" + index +"']");
			var offset = thumbnail.offset(),
				dims = thumbnail[0].getBoundingClientRect();

			return {x: offset.left, y: (offset.top), w: dims.width};
		}
	});
	pswp.init();

	event.preventDefault();
}

function bindHandlers(context) {
	$(".switcher", context).on("click", switcherOnClick);
	$(".stack .block", context).on("click", tabOnClick);
}

function switcherOnClick(event) {
	//debugMessage(this);

	event.preventDefault();

		var type = $(this).hasClass("skill") ? "skill" : "tag",
		id = $(this).attr("data-id");

	switchSelection(type, id, this);

	// modify browser history (and URL)
	if (window.history) {
		if ($("#main").hasClass("project")) // this is a project article page; preserve the path and add context parameters
			window.history.pushState({type: type, id: id}, (type + "/" + id), location.pathname + "?contextType=" + type + "&contextId=" + id);
		else // this is the home page
			window.history.pushState({type: type, id: id}, (type + "/" + id), "/portfolio/" + type + "s/" + id);
	}
}

function tabOnClick(event)
{
	var elementsClickedOn = document.elementsFromPoint(event.clientX, event.clientY),
		tabClicked = $(elementsClickedOn).filter(".leader");
	
	if (tabClicked.length > 0) {
		tabClicked.closest(".stack").find(".selected").removeClass("selected");
		tabClicked.closest(".block").addClass("selected");
	}
}

function scaleProjectTile(toScale) {
    $(toScale).each(function(n, o) {
		log("scaling");
		
		var proj = $(o).closest(".card"); //(".project-tile");
		
		var viewportDims = { width: proj.innerWidth() }

		log("viewportDims.width = " + viewportDims.width)
		
		var scale1, scale2;

        var naturalDims = { 
			width: $(o).outerWidth(), 
			height: $(o).outerHeight()
			}
		
		if (naturalDims.width > viewportDims.width)
			scale1 = (viewportDims.width / naturalDims.width)
		else
			scale1 = 1;

		// if (naturalDims.height > viewportDims.height)
		// 	scale2 = (viewportDims.height / naturalDims.height)
		// else
		// 	scale2 = 1;
		
		// var chosenScale = Math.min(scale1, scale2),
		transformString = "scale("+ scale1 +")"; // translate(-50%, -50%)";
		
		$(o).css("transform", transformString); // vendor-specific prefixes added automatically

		proj.find(".visual").height(naturalDims.height * scale1)
		
    })	
}

function scaleProjectArticle(toScale) {
	if (toScale.length > 0) {
		log("scaling");
		
		var proj = $(toScale).closest(".project-article");

		var viewportDims = {
			width: proj.innerWidth(),
			height: $(window).innerHeight() - $(toScale).position().top - 125
		}

		if (!initialViewportHeight) {
			initialViewportHeight = viewportDims.height;
			// alert ('initialViewportHeight = ' + initialViewportHeight);
		}

		// ensure that vertical height of content is not scaled up when window is made deeper (happens on Chrome iOS when top bar is scrolled out of view)
		viewportDims.height = Math.min(viewportDims.height, initialViewportHeight);
		
		log(viewportDims);
		
		var scale1, scale2, scale3;
		//debugMessage(o);
		var naturalDims = { 
			width: $(toScale).outerWidth(), 
			height: $(toScale).outerHeight()
			}
		
		if (naturalDims.width > viewportDims.width)
			scale1 = (viewportDims.width / naturalDims.width);
		else
			scale1 = 1;

		if (naturalDims.height > viewportDims.height)
			scale2 = (viewportDims.height / naturalDims.height);
		else
			scale2 = 1;
		
		var chosenScale = Math.min(scale1, scale2);

		// use the ratio of viewport width to scaled item with to decide on a horizontal or vertical layout
		var ratio = viewportDims.width / (naturalDims.width * chosenScale);
		if (ratio > 1.5) {
			proj.addClass("horiz-layout").removeClass("vert-layout");
		} else {
			proj.addClass("vert-layout").removeClass("horiz-layout");
			if ((naturalDims.height * chosenScale) > 0.8 * (viewportDims.height))
				chosenScale = (viewportDims.height / naturalDims.height * 0.8);
		}
		

		var transformString = "scale("+ chosenScale +")";
		$(toScale).css("transform", transformString); // vendor-specific prefixes added automatically
		$(toScale).closest(".visual").css("height", (naturalDims.height * chosenScale) + "px");
		$(toScale).closest(".visual").css("width", (naturalDims.width * chosenScale) + "px");
		
		if (scale1 < 1 || scale2 < 1)
		{
			if (scale1 < scale2)
				proj.addClass("fit-width");
			else
				proj.addClass("fit-height");
		}
		
		//debugMessage(proj.find(".text").innerHeight())

	}
}


// thanks to http://www.simonbattersby.com/blog/simple-jquery-image-crossfade/

function cycleImages(){
      var $active = $('.cycler .active');
      var $next = ($active.next().length > 0) ? $active.next() : $('.cycler img:first');
      $next.css('z-index',2);//move the next image up the pile
      $active.fadeOut(1500,function(){//fade out the top image
	  $active.css('z-index',1).show().removeClass('active');//reset the z-index and unhide the image
          $next.css('z-index',3).addClass('active');//make the next image the top one
      });
    }

function log(message) {
	if (window.console)
		debugMessage(message)
}


function switchSelection(type, id) {
	context = {type: type, id: id};

	scrollIntoView($(".collection-title"), 150)
	
	// update current selection array
	currentProjects = (type == "tag") ? tagMap[id].projects : skillMap[id].projects;

	// animate and hide projects
	var hiddenProjects = $(".project-tile:not(." + type + "-" + id + ") .card")
		.animate({top: 40, opacity: 0} ,{
			complete: function() {
				$(this).closest(".project-tile")
					.appendTo("#backstage")
					.find(".card").css({top: "", left: ""});
			}
		})

	// show projects
	$("#backstage .project-tile.doubleWidth." + type + "-" + id)
		.css({top: 0, left: 0})
		.addClass("incoming")
		.appendTo(".sub-group");

	$("#backstage .project-tile." + type + "-" + id)
		.css({top: 0, left: 0})
		.addClass("incoming")
		.insertAfter(".sub-group");

	var incomingCount = $(".incoming").length;

	// animate incoming projects
	$(".incoming .card").each(function(n, o) {
		var thisCard = $(o);
		
		thisCard.css({opacity: 0});

		thisCard.closest(".project-tile").css({left: n*40});

		thisCard.delay(50 * n)
			.animate({top: 0, opacity: 1}, {
				complete: function() {
					$(this).closest(".incoming").removeClass("incoming");
					reshuffleIfReady();
				}
			})
	})

	// load any missing project modules
	for (var i = 0; i < currentProjects.length; i++) {
		if ($(".project-tile#" + currentProjects[i]).length == 0) {
			incomingCount++;

			$("#tiled-area").removeClass("loaded");

			$.get(
				"/portfolio/projects/" + currentProjects[i] + "/tile",
				ajaxLoadSuccess
			)
		}
	}

	//debugMessage("Incoming: " + incomingCount)

	// maybe there are no incoming projects; in which case we need to do a reshuffle once the hiding animation is finished
	if (incomingCount == 0)
		window.setTimeout(reshuffleLayout, 500);

	// finally, update heading text
	if (id == "best" || id == "featured") {
		$(".collection-title").slideUp()
	}
	else if (type == "tag") {
		$(".collection-title").text("Projects tagged ")
			.append($(".tag.switcher[data-id='" + id + "']").first().clone(true))
			.slideDown() // in case it's hidden
	}
	if (type == "skill") {
		$(".collection-title").text("Projects made using ")
			.append($(".skill.switcher[data-id='" + id + "']").first().clone(true))
			.slideDown() // in case it's hidden
	}

}

function ajaxLoadSuccess(html) {
	var newProj = $(html);

	bindHandlers(newProj);

	newProj.addClass("incoming").css({
		position: "absolute", top: 0, left: 100
	});

	if (newProj.hasClass("doubleWidth"))
		newProj.appendTo(".sub-group.doubleWidth");
	else
		newProj.appendTo("#tiled-area");	

	$(".visual img", newProj).on("load", function () {
		// image loaded; flag as such and fire parent .visual load event
		log("IMG load: " + $(this).attr("src"))
		$(this).addClass("loaded");
		$(this).closest(".visual").trigger("load")
	})
	
	$(".visual", newProj).on("load", function() {
		log(".visual load");
		
		var t = $(this);
		
		if (t.find("img:not(.loaded)").length == 0) // all images have loaded
		{
			t.addClass("loaded");
			t.css({visibility: "visible"});
			scaleProjectTile(t.find(".scaleThis"));

			t.closest(".card").animate({top: 0, opacity: 1}, {
				complete: function() {
					$(this).closest(".incoming").removeClass("incoming");
					reshuffleIfReady();
				}
			})
		}
	})
}

function reshuffleIfReady() {
	if ($("#tiled-area .incoming").length == 0) {
		reshuffleLayout();
	}
}

function reshuffleLayout() {

	// maintain the 'unstaged' class on sub-groups whose children are ALL unstaged
	$(".sub-group").each(function(n, subGroup) {
		if ($(subGroup).children(".unstaged").length == $(subGroup).children().length) {
			$(subGroup).addClass("unstaged");
		}
		else {
			$(subGroup).removeClass("unstaged");
		}
	})

	$(".sub-group:not(.unstaged)")
		.masonry('reloadItems', subgroupMasonryOptions)
		.masonry(subgroupMasonryOptions)

	mainMasonryOptions.columnWidth = ($("#tiled-area").find(".singleWidth").length == 0) ?
		".doubleWidth" : ".singleWidth";

	$("#tiled-area")
		.masonry('reloadItems', mainMasonryOptions)
		.masonry(mainMasonryOptions)

	// update context parameters in all links
	$("a.project-tile").each(function() {
		var regex = /\?.*$/,
			thisAnchor = $(this),
			currentHref = thisAnchor.attr("href");

		if (regex.test(currentHref))
			thisAnchor.attr("href", currentHref.replace(regex, "?contexType=" + context.type + "&contextId=" + context.id))
		else
			thisAnchor.attr("href", currentHref + "?contexType=" + context.type + "&contextId=" + context.id)
	})

}

function currentSkills() {
	var result = new Array();

	for(var i = 0; i < currentProjects.length; i++) {
		result = result.merge(projectData[currentProjects[i]].skills);
	}

	return result;
}

function currentTags() {
	var result = new Array();

	for(var i = 0; i < currentProjects.length; i++) {
		result = result.merge(projectData[currentProjects[i]].tags);
	}

	return result;
}

function scrollIntoView(elem, margin) {
	var target = $(elem),
		screenTop = $(window).scrollTop(),
		screenBottom = screenTop + $(window).height(),
		targetTop = target.offset().top,
		targetBottom = targetTop + target.outerHeight(),
		margin = margin || 100;

	// alert ("screenTop=" + screenTop + " screenBottom=" + screenBottom + " targetTop=" + targetTop + " targetBottom=" + targetBottom)

	if ((targetTop - margin) > screenTop && (targetBottom + margin) < screenBottom)
		return; // item is already in view

	if ((targetTop - margin) < screenTop) {
		$("html, body").animate({scrollTop: targetTop - margin});
		return;
	}

	if ((targetBottom + margin) > screenBottom) {
		$("html, body").animate({scrollTop: (targetBottom - $(window).height() + margin)});
		return;
	}
}


Array.prototype.merge = function(b) {
    var a = this.concat(); // clones a

    for(var i = 0; i < b.length; i++) {
        if (a.indexOf(b[i]) == -1)
        	a.push(b[i]);
    }

    return a;
};


function debugMessage(message) {
	if (window.console)
		console.log(message)
}