$(document).ready(function() {
	// viewport = $("#viewport");
	
	$(window).on("resize", function() {
		scaleProjectArticle($(".scaleThis"));
	});
	
	// trick to ensure load event fires in IE on cached images
	$(".visual img").each(function() {
		$(this).attr("src", $(this).attr("src"))
	});
	
	// viewport.width($(window).innerWidth());
	
	$(".visual img").on("load", function () {
		// image loaded; flag as such and fire parent .visual load event
		log("IMG load: " + $(this).attr("src"))
		$(this).addClass("loaded");
		$(this).closest(".visual").trigger("load")
	})
	
	$(".project-article .visual").on("load", function() {
		log("article .visual load");
		
		var t = $(this);
		
		if (t.hasClass("loaded"))
		{
			if (t.hasClass("scaleThis"))
				scaleProjectArticle(t)
			else
				scaleProjectArticle(t.find(".scaleThis"));
		}
		else
		{
			if (t.find("img:not(.loaded)").length == 0) // all images have loaded
			{
				t.addClass("loaded");
				t.css({visibility: "visible"});
				scaleProjectArticle(t.find(".scaleThis"));
				// canvas.trigger("load");
				
			}
		}
	})

	
})

function scaleProjectArticle(toScale) {
	if (toScale.length > 0) {
		log("scaling");
		
		var viewportDims = {
			width: $(toScale).closest(".project-article").innerWidth(),
			height: $(window).innerHeight() - $(toScale).position().top
		}
		
		log(viewportDims);
		
		var scale1, scale2;
		//console.log(o);
		var naturalDims = { 
			width: $(toScale).outerWidth(), 
			height: $(toScale).outerHeight() * 1.05
			}
		
		if (naturalDims.width > viewportDims.width)
			scale1 = (viewportDims.width / naturalDims.width);
		else
			scale1 = 1;

		if (naturalDims.height > viewportDims.height)
			scale2 = (viewportDims.height / naturalDims.height);
		else
			scale2 = 1;
		
		var chosenScale = Math.min(scale1, scale2),
			transformString = "scale("+ chosenScale +")";
		$(toScale).css("transform", transformString); // vendor-specific prefixes added automatically
		$(toScale).closest(".visual").css("height", (naturalDims.height * chosenScale) + "px");
		$(toScale).closest(".visual").css("width", (naturalDims.width * chosenScale) + "px");
		
		var proj = $(toScale).closest(".project-article");
		
		if (scale1 < 1 || scale2 < 1)
		{
			if (scale1 < scale2)
				proj.addClass("fit-width");
			else
				proj.addClass("fit-height");
		}
		
		// use the ratio of viewport width to scaled item with to decide on a horizontal or vertical layout
		var ratio = viewportDims.width / (naturalDims.width * chosenScale);
		if (ratio > 1.3)
			proj.addClass("horiz-layout").removeClass("vert-layout");
		else
			proj.addClass("vert-layout").removeClass("horiz-layout");
	}
}

function log(message) {
	if (window.console)
		console.log(message)
}