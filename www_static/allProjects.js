﻿// DEPRECATED - now coded in allProjects.json

// use class="scaleThis" in the element whose current width is used to set the width of the enclosing project div

var allProjects = 
[
	{
		"id": "mices_ui",
		"folder": "mices",
		"tags": ["mices"],
		"skills": ["vb", "coldfusion"],
		"dates": [2001,2006],
		"content": "<div class=\"visual\"><img src=\"projects/mices/screens.jpg\" class=\"scaleThis centered\" style=\"height: 75%\"/></div>",
		"label": "User interface design",
		"capsuleDesc": "Using Visual Basic as a platform, I designed and developed the on-screen interfaces for both staff and customers of our cybercafé chain. Customers were presented with a very simple “click-to-start” screen with an indication of cost. Staff used an integrated touchscreen-ready till system that showed a visual map of seats and their occupants and allowed management of bill add-ons such as coffee.",
		"thumbnail": {"path": "projects/mices/screens.jpg", "style": "zoom: 0.32; left: -482px; top: -334px; "}
	},
	{
		"id": "windmill_datahost",
		"folder": "windmill",
		"tags": ["logo", "brand", "windmill"],
		"dates": [1999],
		"content": '<div class="vertCentre"><div class="vertCentreContent"><table class="notScaled"><tr ><td rowspan="2"><img src="projects/windmill/datahost_purple.png"/></td><td><img src="projects/windmill/datahost_green.png"/></td></tr><tr><td><img src="projects/windmill/datahost_red.png"/></td></tr></table></div></div>',
		"label": "Logotype system for data product suite",
		"capsuleDesc": "A system of related logotypes for the data hosting services of a company specialising in data cleaning, transformation and analysis."
	},
	{
		"id": "windmill_trueSuite",
		"folder": "windmill",
		"tags": ["logo", "brand", "windmill"],
		"dates": [1999],
		"content": '<div class="vertCentre"><div class="vertCentreContent"><img src="projects/windmill/trueSuite.png" class="notScaled"/></div></div>',
		"label": "Logotype system for data product suite",
		"capsuleDesc": "A system of related logotypes for the software products of a company specialising in data cleaning, transformation and analysis."
	},
	{
		"id": "xko",
		"folder": "xko",
		"tags": ["logo", "brand"],
		"dates": [2001, 2003],
		"content": '<div class="vertCentre"><div class="vertCentreContent"><table><tr><td><img src="projects/xko/logo.png"/></td></tr><tr><td><img src="projects/xko/alphabet_small.png" class="thisWidth"/></td></tr></table></div></div>',
		"label": "Logo and branding for a software company",
		"capsuleDesc": "This brand package for the ERP software vendor included a logo, stationery design, and a full custom typeface as a brand asset that could be used to build product sub-brands."
	},
	{
		"id": "ryzen_logo",
		"folder": "ryzen",
		"tags": ["logo", "brand"],
		"dates": [2013],
		"content": '<div class="vertCentre"><div class="vertCentreContent"><table class="notscaled" style="background-color: white; padding: 25px"><tr><td class="rangeLeft"><div class="caption">before</div><img src="projects/ryzen/before.png" style="padding-top: 14px"/></td><td style="padding-left: 40px" class="rangeLeft"><div class="caption">after</div><img src="projects/ryzen/logo-refresh.png"/></td></tr></table></div></div>',
		"label": "Logo refresh for a software company",
		"capsuleDesc": "The original 3D-rendered logo was redrawn in a flatter, more contemporary rounded style. This brings the logo up-to-date and also gives it more visual impact, especially when displayed small at the head of a website."
	},
	{
		"id": "jazzNo1",
		"folder": "JazzNo1",
		"tags": ["print", "cover"],
		"dates": [2011],
		"content": '<div style="height: 100%"><img src="projects/JazzNo1/3D_render.png" style="height: 80%; width: auto; margin-top: 10%; margin-bottom: 10%" class="scaleThis"/></div>',
		"label": "CD cover",
		"capsuleDesc": "This compilation CD cover invokes a graphical style typical of record sleeve design from the 60’s, with a deliberate choice of colour palette, and the slightly mis-registered colour fields which were a feature of the over-printing techniques popular at the time."
	},
	{
		"id": "longwellPress",
		"folder": "longwell-press",
		"tags": ["logo", "brand", "website"],
		"dates": [2008],
		content: '<div class="vertCentre"><div class="vertCentreContent"><img src="projects/longwell-press/new-logo_02.png" class="notScaled"/></div></div>',
		"label": "Logotype for book publisher",
		"capsuleDesc": "A logotype for a small Cotswold-based publisher of language training books",
		"externalLink": "http://www.longwellpress.com"
	},
	{
		"id": "wedding",
		"folder": "wedding",
		"tags": ["invite", "print"],
		"dates": [2013],
		"content" : '<div style="height: 100%"><img src="projects/wedding/photo.jpg" style="height: 80%; margin-top: 10%; margin-bottom: 10%; width: auto" class="scaleThis"/></div>',
		"label": "Wedding invitation",
		"capsuleDesc": "My design for this wedding invitation was a hommage to 19th-century poster design, complete with overpunctuation and an insane mixture of typefaces and emphases within a single rambling sentence. The invitation was designed to stylistically complement the venue for the reception, a well-preserved Victorian London Gin Palace."
	},
	{
		"id": "100years",
		"folder": "100years",
		"tags": ["card", "print"],
		"dates": [2004],
		"content" : '<div class="vertCentre"><div class="vertCentreContent"><table class="notscaled" style="border-spacing: 15px"><tr><td><img src="projects/100years/70.png"/></td><td><img src="projects/100years/30.png"/></td></tr><tr><td colspan="2"><img src="projects/100years/100.png" style="position: relative; top: 10px"/></td></tr></table></div></div>',
		"label": "Birthday cards",
		"capsuleDesc": "A pair of cards for the joint birthday celebrations of a 70-year-old born in the 30’s and a 30-year-old born in the 70’s."
	},
	{
		"id": "xmas2012",
		"folder": "xmas2012",
		"tags": ["card", "print"],
		"dates": [2012],
		content: '<div style="height: 100%"><img src="projects/xmas2012/photo.jpg" style="height: 74%; width: auto; margin-top: 13%; margin-bottom: 13%" class="scaleThis"/></div>',
		"label": "Christmas card",
		"capsuleDesc": "A Christmas card for friends and family"
	},
	{
		"id": "cocktailParty",
		"folder": "cocktail-party",
		"tags": ["card", "invite", "print"],
		"dates": [1999],
		"content" : '<div style="height: 100%"><img src="projects/cocktail-party/cocktail.png" style="height: 80%; width: auto; margin-top: 10%; margin-bottom: 10%" class="scaleThis"/></div>',
		"label": "Party invitation",
		"capsuleDesc": "A party invitation inspired by a book of cocktail recipes from the 1950s (the zeitgeist decade for cocktail parties)"
	},
	/*
	{
		"id": "computerArts",
		"folder": "computer-arts",
		"tags": ["website", "review"],
		"dates": [1999],
		"content" : '<div style="height: 100%"><img src="projects/computer-arts/photo.jpg" style="height: 80%; width: auto; margin-top: 10%; margin-bottom: 10%" class="scaleThis"/></div>',
		"label": "Early web design",
		"capsuleDesc": "My original online portfolio from 1999 was featured in an issue of Computer Arts Magazine."
	},
	*/
	{
		"id": "dontLookNow",
		"folder": "dont-look-now",
		"tags": ["poster", "print", "icc"],
		"dates": [2012],
		content: '<img src="projects/dont-look-now/poster.png" style="height: 100%; width: auto"/>',
		"label": "Film screening poster",
		"capsuleDesc": "A poster publicising the screening of a film by an Oxford-based film society. The film, set amongst the canals of Venice, deals with the reflections between the real and the supernatural. My poster attempts to hint at the themes of the film without giving too much away."
	},
	{
		"id": "italianForBeginners",
		"folder": "italian-for-beginners",
		"tags": ["poster", "print", "icc"],
		"dates": [2012],
		content: '<img src="projects/italian-for-beginners/poster.png" style="height: 100%; width: auto"/>',
		"label": "Film screening poster",
		"capsuleDesc": "A poster publicising the screening of a film by an Oxford-based film society. This film revolves around the goings-on at an Italian language adult education class in Denmark. (If you know your furniture designers, you'll know why I picked this particular design of chair.)"
	},
	{
		"id": "beatThatMyHeartSkipped",
		"folder": "beat-that-my-heart-skipped",
		"tags": ["poster", "print", "icc"],
		"dates": [2012],
		content: '<img src="projects/beat-that-my-heart-skipped/poster.jpg" style="height: 100%; width: auto"/>',
		"label": "Film screening poster",
		"capsuleDesc": "A poster publicising the screening of a film by an Oxford-based film society: the protagonist in this film is a Parisian underworld gangster with hidden talents as a classical pianist."

	},
	{
		"id": "godot",
		"folder": "Godot",
		"tags": ["poster", "print"],
		"dates": [2000],
		content: '<img src="projects/Godot/Godot1.png" style="height: 100%; width: auto" class="thisWidth"/>',
		"label": "Theatre poster",
		"capsuleDesc": "The design was used around Oxford to promote a production of Becket’s “Waiting for Godot” at the Burton Taylor Theatre. It’s influenced by Otto Neurath’s iconic ISOTYPE information graphics from the 1930’s."

	},
	{
		"id": "icc_web",
		"folder": "icc",
		"tags": ["web", "icc"],
		"dates": [2010],
		content: '<div style="height: 100%"><img src="projects/icc/webGrab1.jpg" style="height: 80%; width: auto; margin-top: 10%; margin-bottom: 10%" class="scaleThis"/></div>',
		"label": "Website design",
		"capsuleDesc": "My complete redesign for this film society’s website includes dynamically-updated termly listings, a searchable DVD library index, and a subscribable calendar that automatically brings the club’s screenings schedule to members’ Outlook calendars and other desktop calendar apps.",
		"externalLink": "http://www.internationalcinemaclub.org.uk"
	},
	/*
	{
		"id": "icc_workflow",
		"folder": "icc",
		"tags": ["web", "icc"],
		"dates": [2010],
		content: '<div class="vertCentre"><div class="vertCentreContent"><img src="projects/icc/ICC-workflow.png" class="notScaled"/></div></div>',
		"label": "Website design",
		"capsuleDesc": "My complete redesign for this film society’s website includes dynamically-updated termly listings, a searchable DVD library index, and a subscribable calendar that automatically brings the club’s screenings schedule to members’ Outlook calendars and other desktop calendar apps.",
		"externalLink": "http://www.internationalcinemaclub.org.uk"
	},
	*/
	{
		"id": "switch_web",
		"folder": "switch",
		"tags": ["web", "switch"],
		"dates": [2011, 2013],
		content: '<div style="height: 100%"><img src="projects/switch/webGrab1.jpg" style="height: 80%; width: auto; margin-top: 10%; margin-bottom: 10%" class="scaleThis"/></div>',
		"label": "Website design",
		"capsuleDesc": "The design for Switch Research’s website is deliberately kept clean and light so as not to detract from the marketing message. I provided my own abstract diagrammatic illustrations that form part of the company’s brand identity. The site includes a popular ‘tech switch’ device that allows the revealing or hiding of technical information depending on the audience’s preferences.",
		"externalLink": "http://www.switchresearch.com"
	},
	{
		"id": "switch_webUi",
		"folder": "switch",
		"tags": ["webUi", "switch"],
		"dates": [2010],
		content: '<div style="height: 100%"><img src="projects/switch/webGrab3.jpg" style="height: 80%; width: auto; margin-top: 10%; margin-bottom: 10%" class="scaleThis"/></div>',
		"label": "Product interface design",
		"capsuleDesc": "The management information interface for this product uses modern web technologies including XHTML and SVG to provide dynamic graphical information displays. It was actually inspired by the very forward-looking interface designs featured in Stanley Kubrick’s film “2001 – A Space Odyssey”!",
	},
	{
		"id": "mices_active_desktop",
		"folder": "mices",
		"tags": ["webUi", "mices"],
		"dates": [2005],
		content: '<div style="height: 100%"><img src="projects/mices/activeDesktop.jpg" style="height: 80%; width: auto; margin-top: 10%; margin-bottom: 10%" class="scaleThis"/></div>',
		"label": "Welcome screen / active desktop",
		"capsuleDesc": "The combined desktop and welcome screen for workstations in this cybercafé was implemented using web technologies. Desktops were context-sensitive to the shop location and also helped collect usage data (such as click-throughs) for shop management analysis.",
	}
];