/* global $ */

var viewportDims, canvas, viewport, projects, focussedProject;
var swiping = false;

$(document).ready(function() {
	projects = $(".project");
	// tagSwitchers = $(".tagSwitcher");
	// commentsLinks = $(".commentsLink");
	canvas = $("#canvas");
	viewport = $("#viewport");
	
	focussedProject = projects.eq(0); // just for the moment
	projects.on("click", clickOnProject);

	// trick to ensure load event fires in IE on cached images
	$(".visual img").each(function() {
		$(this).attr("src", $(this).attr("src"))
	});
	
	$(window).on("resize", resize);

	$(".visual img").on("load", function () {
		// image loaded; flag as such and fire parent .visual load event
		log("IMG load: " + $(this).attr("src"))
		$(this).addClass("loaded");
		$(this).closest(".visual").trigger("load")
	})

	$(".visual").on("load", function() {
		log(".visual load");
		
		var t = $(this);
		
		if (t.hasClass("loaded"))
		{
			if (t.hasClass("scaleThis"))
				scale(t)
			else
				scale(t.find(".scaleThis"));
		}
		else
		{
			if (t.find("img:not(.loaded)").length == 0) // all images have loaded
			{
				t.addClass("loaded");
				t.css({visibility: "visible"});
				scale(t.find(".scaleThis"));
				canvas.trigger("load");
			}
		}
	})
	
	canvas.on("load", function() {
		if (!canvas.hasClass("loaded"))
		{
			if (canvas.find(".visual:not(.loaded)").length == 0) // all projects have loaded
			{
				console.log("all projects loaded");
				canvas.addClass("loaded");
				if (focussedProject)
					canvas.css({left: calcCanvasOffset(focussedProject)});
			}
		}
	})
	
	canvas.draggable({axis: "x", start: canvasStartSwipe, stop: canvasEndSwipe});

	resize();
	
})

function resize() {
    viewportDims = { width: $(window).innerWidth(), height: $(window).innerHeight() };
	// $(".project").width(viewportDims.width);
    
	log("Resize w=" + viewportDims.width + " h=" + viewportDims.height)

	$(".visual").trigger("load");
	
	if (focussedProject)
		canvas.css({left: calcCanvasOffset(focussedProject)});
}

function scale(toScale) {
    $(toScale).each(function(n, o) {
		log("scaling")
		
		var scale1, scale2;
        //console.log(o);
        var naturalDims = { 
			width: $(o).outerWidth() * 1.2, 
			height: $(o).outerHeight() * 1.15
			}
		
		if (naturalDims.width > viewportDims.width)
			scale1 = (viewportDims.width / naturalDims.width)
		else
			scale1 = 1;

		if (naturalDims.height > viewportDims.height)
			scale2 = (viewportDims.height / naturalDims.height)
		else
			scale2 = 1;
		
		var chosenScale = Math.min(scale1, scale2),
			transformString = "scale("+ chosenScale +") translate(-50%, -50%)";
		$(o).css("transform", transformString); // vendor-specific prefixes added automatically
		
		var proj = $(o).closest(".project");
		proj.width(naturalDims.width * chosenScale)
		
    })
}

function clickOnProject()
{
	if (!swiping)
	// 	if ($(this).is(focussedProject))
	// 		if (focussedProject.attr("showingComments"))
	// 			hideComments(focussedProject);
	// 		else
	// 			toggleFocussedProjectInfo();
	// 	else
		scrollToProject(this);
}

// move the canvas horizontally so that the given element (a project) is centered
function scrollToProject(elem)
{
	if (focussedProject)
		if (focussedProject.attr("showingInfo"))
			hideProjectInfo(focussedProject);
		
	pr = $(elem);
	
	focussedProject = pr;
	canvas.animate({left: calcCanvasOffset(elem)});
	// hideEndcaseNavButtons();
	location.hash = "#" + pr.attr("pid");
	
	// projectInfo is revealed after a delay
	// window.setTimeout(function(){revealProjectInfo(focussedProject)}, 2000);
}

function scrollToRight()
{
	if (focussedProject)
	{
		//revealCanvas();
		
		next = focussedProject.next();
		if (next.exists())
			scrollToProject(next);
		else
			scrollToProject(focussedProject); // creates an Apple-style bounce-at-end
	}
}

function scrollToLeft()
{
	if (focussedProject)
	{ 
		//revealCanvas();
		
		prev = focussedProject.prev();
		if (prev.exists())
			scrollToProject(prev);
		else
			scrollToProject(focussedProject); // creates an Apple-style bounce-at-end
	}
}

// calculates the canvas offset required to centre elem within the viewport
function calcCanvasOffset(elem)
{
	var p = $(elem);
	return -(p.position().left-((viewportDims.width-p.outerWidth(true))/2));
}

function canvasStartSwipe(event, ui)
{
	swiping = true;
}

function canvasEndSwipe(event, ui)
{
	if (ui.position.left < ui.originalPosition.left)
		scrollToRight();
	else
		scrollToLeft();
		
	window.setTimeout(function() { swiping = false; }, 300);
}

$.fn.exists = function () {
    return this.length !== 0;
}

function log(message) {
	$("#debug").append(message + "\n")
}