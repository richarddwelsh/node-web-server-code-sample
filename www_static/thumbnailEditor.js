$("document").ready(startup);

// populated dynamically in Jade template
// var xOffset = 0;
// var yOffset = 0;
// var zoomFactor = 1;

function startup()
{
	$("#sourceForm").on("submit", loadImage );
	$("#zoom").on("change", adjustImage );
	$("#placeholder").draggable({drag: dragPlaceholder});
	$("#updateButton").on("click", update );

	if (thumbDefined) {
		loadImage();
		adjustImage();
	}


}

function update() {
	$(this).attr("disabled", "disabled");

	$.ajax({
		method: "POST",
		url: "updateThumbnail",
		data: JSON.stringify({ 
			id: id,
			xOffset: xOffset,
			yOffset: yOffset,
			zoomFactor: zoomFactor,
			path: path
		 }),
		dataType: "json",
		contentType: 'application/json; charset=utf-8',
		success: updateSuccess
	})
}

function updateSuccess(data) {
	location.reload();
}

function loadImage()
{
	path = $("#sourcePath").val();
	$("#thumbnail").attr("src", "/" + path);
	$("#full").attr("src", "/" + path);
	return false;
}

function adjustImage()
{
	//$("#thumbnail").css({"zoom": $("#zoom").val()});
	//$("#thumbnail").css({"-moz-transform": "scale(" + $("#zoom").val() + ")"});
	zoomFactor = Number($("#zoom").val());
	$("#thumbnail").css("transform", "translate(" + Math.floor(xOffset * zoomFactor) + "px, " + Math.floor(yOffset * zoomFactor) +"px) scale(" + zoomFactor + ")");

	$("#placeholder").css({
		width: 80*(1/($("#zoom").val())), 
		height: 80*(1/($("#zoom").val())),
		left: -xOffset,
		top: -yOffset
		});
	$("#result").text(
		'"thumb": {"path": "' + $("#sourcePath").val() + '", "xOffset": ' + xOffset + ', "yOffset": ' + yOffset + ', "zoomFactor": ' + zoomFactor + '}'
	);
}

function dragPlaceholder(event, ui)
{
	//$("#thumbnail").css({left: -(ui.position.left), top: -(ui.position.top)});
	xOffset = -(ui.position.left);
	yOffset = -(ui.position.top);
	$("#thumbnail").css("transform", "translate(" + Math.floor(xOffset * zoomFactor) + "px, " + Math.floor(yOffset * zoomFactor) +"px) scale(" + zoomFactor + ")");

	$("#result").text(
		'"thumb": {"path": "' + $("#sourcePath").val() + '", "xOffset": ' + xOffset + ', "yOffset": ' + yOffset + ', "zoomFactor": ' + zoomFactor + '}'
	);
}
