// Contains some code from TADA's Charts.js
// *** re-written for D3.js v4

// convenience method for adding transform: translate attributes 
d3.selection.prototype.translate = function (x, y) { return this.attr("transform", "translate(" + x + "," + y + ")") }

// Dims class: contains width, height, overallWidth and overallHeight, and margins of a chart
// you can specify either overall width/height or inner width/height and the other will be calculated from the margins
function Dims(options) {
    this.margin = options.margin || { top: 0, bottom: 0, left: 0, right: 0 };
 
    this.physicalWidth = options.physicalWidth || 180; // 180mm about right for full-width A4 portrait
    this.physicalUnits = options.physicalUnits || "mm";

    if (options.width) {
        this.width = options.width;
        this.overallWidth = this.width + this.margin.left + this.margin.right;
    } else {
        this.overallWidth = options.overallWidth;
        this.width = this.overallWidth - this.margin.left - this.margin.right;
    }

    if (options.height) {
        this.height = options.height;
        this.overallHeight = this.height + this.margin.top + this.margin.bottom;
    } else {
        this.overallHeight = options.overallHeight;
        this.height = this.overallHeight - this.margin.top - this.margin.bottom;
    }
}

// call this method on a selection (usually of one empty 'svg' element) to set up, dimension and return the 'g' element that will contain the actual chart
// e.g. var chart = d3.selectAll("svg").setupChartCanvas(dims);
d3.selection.prototype.setupChartCanvas = function (dims) {
    return this.attr("viewBox", "0 0 " + dims.overallWidth + " " + dims.overallHeight)
    .attr("width", dims.physicalWidth + dims.physicalUnits)
    .attr("height", Math.round(dims.physicalWidth * (dims.overallHeight / dims.overallWidth)) + dims.physicalUnits)
    .append("g") // the main container of the chart, inside the margins
        .attr('class', 'dataspace')
        .translate(dims.margin.left, dims.margin.top);
}
