/* global $ */

var allProjects;
$.getJSON("allProjects.json", function(data) { allProjects = data; });

var tags;
$.getJSON("tags.json", function(data) { tags = data; });

var canvas;
var viewport;
var projects;
var tagSwitchers;
var commentsLinks;
var focussedProject;
var leftButton;
var rightButton;
var swiping = false;
var titleBoxStuckOn = false;
var hoveringOnDog = false;

$(document).ready(setup);

function setup()
{
	document.ontouchmove = function(e) {e.preventDefault()}; // for iOS
	
	$("#behance,#mail").on("mouseover", mouseover).on("mouseout", mouseout);
	
	projects = $(".projectDiv");
	tagSwitchers = $(".tagSwitcher");
	commentsLinks = $(".commentsLink");
	canvas = $("#canvas");
	viewport = $("#viewport");
	leftButton = $("#leftButton");
	rightButton = $("#rightButton");
	
	$(window).on("resize", resizeWindow);
	$(window).on("load", pageLoaded );
	
	$(".projectComments").on("load", commentsLoaded);
	
	// dog = "digitally-originated-graphic", i.e. logo in corner
	$("#dog").on("mouseenter", dogEnter );
	$("#dog").on("click", clickOnDog );
	// delayed mouseleave event so that pointer can move to infobox wihtout it disappearing
	$("#dog").on("mouseleave", function(){window.setTimeout(dogLeave, 300)} );
	// titlebox = name & email box that appears when hovering over dog
	$("#titleBox").on("mouseenter", titleBoxEnter );
	$("#titleBox").on("mouseleave", titleBoxLeave );
	
	// simulate anchor change by scrolling to appropriate project
	$(window).on("hashchange", scrollToAnchoredProject );
	
	// keyboard shortcuts
	Mousetrap.bind("left", scrollToLeft);
	Mousetrap.bind("right", scrollToRight);
	Mousetrap.bind("enter", toggleFocussedProjectInfo);
	Mousetrap.bind("esc", function(){hideProjectInfo(focussedProject)});
	
	projects.click(clickOnProject);
	
	leftButton.on("click", scrollToLeft);
	rightButton.on("click", scrollToRight);
	
	tagSwitchers.on("click", clickOnTagSwitcher);
	commentsLinks.on("click", clickOnCommentsLink);
	
	$("#leftButton, #rightButton").hover(
		function() { $(this).css({opacity: "1"}); },
		function() { $(this).css({opacity: ""}); }
		);
	
	// tablet-style dragging of canvas left & right
	canvas.draggable({axis: "x", start: canvasStartSwipe, stop: canvasEndSwipe, cancel: ".showingComments"});
	
	// appendProjectDiv(allProjects[0]);
	// scrollToProject(projects.first());
	
	if (location.search != "")
		highlightTagSwitcher(location.search.substring(1));
}

// clear the canvas and populate it with projects tagged by tag
function showProjectsByTag(tag)
{
	canvas.empty();
	$("#thumbStrip").empty();
	//canvas.css({height: "100px", visibility: "hidden"});
	
	$(allProjects).each(function(n, o) { appendProjectDiv(o, tag) });
	
	window.setTimeout(resizeWindow, 375);
	
	if (location.search != ("?" + tag))
		if (window.history.pushState)
			window.history.pushState(null, "", "?" + tag + location.hash);
	
	window.setTimeout(scrollToAnchoredProject, 700);
	
	highlightTagSwitcher(tag);
}

// tagSwitchers in the tagMenu are higkighted by a thick underline
function highlightTagSwitcher(tag)
{
	var justTopTagSwitchers = tagSwitchers.filter(
		function() { return $(this).parent("#tagMenu").exists(); }
		);

	ts = justTopTagSwitchers.filter("[tag='" + tag + "']");
	justTopTagSwitchers.css({"border-bottom-color": "black"});

	if (ts.exists())
		ts.css({"border-bottom-color": ts.css("color")});
}

function dogEnter()
{
	$("#titleBox").fadeIn();
	hoveringOnDog = true;
}

function dogLeave()
{
	hoveringOnDog = false;
	if (!titleBoxStuckOn)
	{
		$("#titleBox").fadeOut();
	}	
}

function titleBoxEnter()
{
	titleBoxStuckOn = true;
}

function titleBoxLeave()
{	
	titleBoxStuckOn = false;
	window.setTimeout(
		function() {if (!hoveringOnDog) $("#titleBox").fadeOut();},
		300
		);
}

function pageLoaded()
{
	log("page loaded");
	resizeWindow();
	scrollToAnchoredProject();
}

function resizeWindow()
{
	log ("resizing window");
	
	$("#debug").text($(window).outerWidth() + "×" + $(window).outerHeight());
	
	canvas.css({"height": "auto", "bottom": "0", "visibility": "visible"}); /* just because of Chrome */
	projects.each(resizeProject);
}

// adjusts mragins of project in proportion to window size, and fits project with to scaled item with (element with scaleThis class is used to define withs of project)
function resizeProject(n, elem)
{
	pr = $(elem);

	log("resizing " + pr.attr("pid"));
	// find schaleThis element, of if not exsting, first child element is used
	scaledItem = pr.find(".scaleThis").exists() ? pr.find(".scaleThis") : pr.children();
	projectInfo = pr.find(".projectInfo");
	
	// if item too wide for viewport, fit width to viewport
	if (scaledItem.outerWidth() > viewport.width())
		pr.width(viewport.width());
	else
		pr.width(scaledItem.outerWidth());
			
	// nicely proportioned margins
	pr.css({"margin-left": (canvas.height() / 10), "margin-right": (canvas.height() / 10)});

	// if this is the focussed project, readjust horizontal position of convas so that it is centered
	if (focussedProject)
		canvas.css({left: calcCanvasOffset(focussedProject)});
}

function canvasStartSwipe(event, ui)
{
	swiping = true;
}

function canvasEndSwipe(event, ui)
{
	if (ui.position.left < ui.originalPosition.left)
		scrollToRight();
	else
		scrollToLeft();
		
	window.setTimeout(function() { swiping = false; }, 300);
}

function clickOnTagSwitcher()
{
	revealCanvas();
	
	showProjectsByTag($(this).attr("tag"));
	return false;
}

function revealCanvas()
{
	if ($("#intro").css("display") != "none")
	{
		$("#intro").fadeOut();
		$("#dog").fadeIn();
	}
	
	if ($("#canvas").css("display") == "none")
		$("#canvas").fadeIn();
}

function clickOnDog()
{
	$("#dog").fadeOut();
	$("#titleBox").fadeOut();
	$("#intro").fadeIn();
	$("#canvas").fadeOut();
}

function clickOnProject()
{
	if (!swiping)
		if ($(this).is(focussedProject))
			if (focussedProject.attr("showingComments"))
				hideComments(focussedProject);
			else
				toggleFocussedProjectInfo();
		else
			scrollToProject(this);
}

function toggleFocussedProjectInfo()
{
	if (focussedProject.attr("showingInfo"))
		hideProjectInfo(focussedProject);
	else
		revealProjectInfo(focussedProject);
}

// move the canvas horizontally so that the given element (a project) is centered
function scrollToProject(elem)
{
	if (focussedProject)
		if (focussedProject.attr("showingInfo"))
			hideProjectInfo(focussedProject);
		
	pr = $(elem);
	
	focussedProject = pr;
	canvas.animate({left: calcCanvasOffset(elem)});
	hideEndcaseNavButtons();
	location.hash = "#" + pr.attr("pid");
	
	// projectInfo is revealed after a delay
	window.setTimeout(function(){revealProjectInfo(focussedProject)}, 2000);
}

function scrollToAnchoredProject()
{
	var anchoredProj = projects.filter("[pid='" + location.hash.substring(1) +"']");
	if (anchoredProj.exists())
		scrollToProject(anchoredProj.first());
	else if (projects.first().exists())
		scrollToProject(projects.first());
}

function scrollToRight()
{
	if (focussedProject)
	{
		revealCanvas();
		
		next = focussedProject.next();
		if (next.exists())
			scrollToProject(next);
		else
			scrollToProject(focussedProject); // creates an Apple-style bounce-at-end
	}
}

function scrollToLeft()
{
	if (focussedProject)
	{ 
		revealCanvas();
		
		prev = focussedProject.prev();
		if (prev.exists())
			scrollToProject(prev);
		else
			scrollToProject(focussedProject); // creates an Apple-style bounce-at-end
	}
}

// calculates the canvas offset required to centre elem within the viewport
function calcCanvasOffset(elem)
{
	var p = $(elem);
	return -(p.position().left-((viewport.width()-p.outerWidth(true))/2));
}

// generates an entire project div in the DOM and appends it to the canvas, from JSON project profile object
function appendProjectDiv(projectProfile, tagFilter)
{
	if (tagFilter && ($.inArray(tagFilter, projectProfile.tags) >= 0 || $.inArray(tagFilter, projectProfile.skills) >=0))
	{
		log("Appending project: " + projectProfile.id);
		
		var newDiv = $("<div>", {"class": "projectDiv", "pid": projectProfile.id}).append(projectProfile.content);
		
		if (projectProfile.capsuleDesc)
			newDiv.append(
				$("<div>", {"class": "projectInfo"})
					.append($("<span>", {"class": "label", text: projectProfile.label}))
					.append($("<span>", {"class": "year", text: ", " + projectProfile.dates[0] + (projectProfile.dates.length > 1 ? ("–" + projectProfile.dates[1]) : "") }))
					.append($("<div>", {"class": "capsuleDesc"}).append(projectProfile.capsuleDesc))
				);
		
		if (projectProfile.externalLink)
			newDiv.find(".projectInfo").append(
				$("<div>", {"class": "externalLink"})
					.append($("<a>", {"href": projectProfile.externalLink, text: projectProfile.externalLink, target: "new"}))
					);
					
		newDiv.find(".projectInfo").append(
				$("<div>", {"class": "tags"})
			);
			
/*		newDiv.find(".tags").append(
			$("<a>", {"class": "commentsLink"})
				.append(symbol("phylactere")).append("Comments")
				.on("click", clickOnCommentsLink)
				);
*/			
		$.each(projectProfile.tags, function(n, t) {
			newDiv.find(".tags").append(
				$("<a>", {
					tag: t, 
					href: "?" + t, 
					"class": ("tagSwitcher " + classNameForTag(t))
					}).append(symbol("tag")).append(tags[t].desc)
						.on("click", clickOnTagSwitcher)
				);
			});

/*					
		newDiv.append(
			$("<iframe>", {seamless: "true", scrolling: "no", "class": "projectComments", src: ("/wordpress/" + (projectProfile.comments ? projectProfile.comments :  projectProfile.id))})
			);
*/		
		newDiv.find("img").on("load", imageHasLoaded );
		newDiv.find("iframe").on("load", commentsLoaded );
	
		canvas.append(newDiv);
		newDiv.on("click", clickOnProject);
		projects = $(".projectDiv");
	
		// var newThumb = $("<div>", {class: "thumb", pid: projectProfile.id})
		//.append($("<img>", {src: newDiv.find("img").first().attr("src")}));
		// $("#thumbStrip").append(newThumb);
		
		return newDiv;
	}
}

// some tags have colours associated with them; others default to spec_undefined class
function classNameForTag(tag)
{
	if (tags[tag])
		if (tags[tag].colour || tags[tag].colour==0)
			return "spec" + tags[tag].colour;
		else
			return "spec_undefined";
	else
		return "spec_undefined";
}

// returns a clone of the symbol SVG element from a hidden library of SVG symbols in the HTML
function symbol(symbolId)
{
	return $("#symbolOriginals *[symbol='" + symbolId + "']").clone(true);
}

function imageHasLoaded()
{
	log("image loaded: " + $(this).attr("src"));
	var projectThatHasLoaded = $(this).parents(".projectDiv");
	resizeProject(null, projectThatHasLoaded); // needed to display and scale projectDiv once image(s) loaded
}

function hideEndcaseNavButtons()
{
	if (focussedProject.is(projects.first()))
		leftButton.hide(); else leftButton.show();
		
	if (focussedProject.is(projects.last()))
		rightButton.hide(); else rightButton.show();
}

function revealProjectInfo(projectDiv)
{
/*
	log("projectDiv.height() = " + projectDiv.outerHeight());
	log("projectInfo.height() = " + projectDiv.find(".projectInfo").outerHeight());
	*/
	var projectItself = projectDiv.children().first();
	var projectInfo = projectDiv.find(".projectInfo");
	
	var scaleDownHeight = projectDiv.outerHeight() - projectInfo.outerHeight() + 3; // 20px just for good measure
	projectItself.animate({height: scaleDownHeight + "px"});
	
	projectDiv.attr("showingInfo", true);
	projectDiv.addClass("showingInfo");
}

function hideProjectInfo(projectDiv)
{
	if (focussedProject.attr("showingComments"))
		hideComments(projectDiv);

	projectDiv.children().first().animate({height: "100%"});
	projectDiv.removeAttr("showingInfo");
	projectDiv.removeClass("showingInfo");
}

function clickOnCommentsLink(e)
{
	e.stopPropagation();

	var thisProject = $(this).parents(".projectDiv");

	if (thisProject.attr("showingComments"))
		hideComments(thisProject);
	else
		revealComments(thisProject);
}

function revealComments(projectDiv)
{
	projectDiv.attr("showingComments", true);
	projectDiv.addClass("showingComments");
	
	projectDiv.animate({"scrollTop": "600px"});
}

function hideComments(projectDiv)
{
	projectDiv.removeAttr("showingComments");
	projectDiv.removeClass("showingComments");
	
	projectDiv.animate({"scrollTop": "0"});
}

function mouseover()
{
	$(this).css({opacity: 1.0});
}

function mouseout(id)
{
	$(this).css({opacity: 0.6});
}

function commentsLoaded()
{
	// "this" should be an iframe 
	var iframe = $(this);

	log("Comments loaded: " + iframe.attr("src"));

	iframe.css({height: (iframe.contents().find("body").height() + 75) + "px"});
}

$.fn.exists = function () {
    return this.length !== 0;
}

function log(string) {
	if (window.console)
		console.log(string);
}