// JS to support project-specific pages

// TADA

var tadaStep = 1, tadaPositions = [-10, 200, 400];
var tadaDuration = 600;

$(document).ready(function() {
    window.setInterval(tadaCycle, 3000)
})

function tadaCycle() {
    tadaStep += 1; if (tadaStep == 4) tadaStep = 1;

    $('#tada_merge').animate({left: tadaPositions[tadaStep-1]}, tadaDuration)
    $('#tada_merge' + tadaStep).fadeIn(tadaDuration)
    $('.tada_merge').not('#tada_merge' + tadaStep).fadeOut(tadaDuration)
}