# node-web-server

-----

## Linode

Control panel: <https://cloud.linode.com/linodes/18272850>

Public IP address: `176.58.126.95`

## Firewall (`iptables`)

Useful tutorial: <https://www.hostinger.ph/tutorials/iptables-tutorial>

Config saved in `iptables.txt` and also in `/etc/iptables.firewall.rules`

## Service manager (`systemd`)

Useful tutorial: <https://wiki.debian.org/systemd>

Service name: `node-web-server.service`

Status info:
```
sudo systemctl status node-web-server.service
```

Configured in `node-web-server.service` and also in `/etc/systemd/system/node-web-server.service`

## Domain name

Control panel: <https://usercontrol.co.uk/cp-login.cgi?site=bbonline>

-----

## MongoDB

Cloud: <https://cloud.mongodb.com/v2/5e2d8a35ff7a25715f03912b>

## Firebase

Project Console: <https://console.firebase.google.com/project/welshdesign-data/overview>