const Promise = require('bluebird'),
	fs = Promise.promisifyAll(require('fs'));

const PASSWD_DIR = (process.env.HOME || process.env.HOMEPATH ||
	process.env.USERPROFILE) + '/.credentials/';
const PASSWD_PATH = PASSWD_DIR + 'neo4j_credentials.json';

const neo4j = require('neo4j-driver').v1;

let driver;

fs.readFileAsync(PASSWD_PATH)
	.then( 
		json => {
			const creds = JSON.parse(json);
			driver = neo4j.driver(creds.uri, neo4j.auth.basic(creds.username, creds.password));
		},
		err => console.log(err)
	)


exports.cypher = function(query) {
	const session = driver.session();

	return new Promise(
		(resolve, reject) => {
			session.run(query)
				.then(result => {
					session.close();
					resolve(result);
				})
				.catch(err => {
					reject(err);
				})
		}
	);
}
