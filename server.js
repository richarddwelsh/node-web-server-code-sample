/* global next */
/* global __dirname */

var fs = require("fs");
var http = require("http");
var https = require("https");

var credentialsDir = (process.env.HOME || process.env.HOMEPATH || process.env.USERPROFILE) + '/.credentials/';
var privateKey  = fs.readFileSync(credentialsDir + 'privkey.pem', 'utf8');
var certificate = fs.readFileSync(credentialsDir + 'fullchain.pem', 'utf8');

var credentials = {key: privateKey, cert: certificate};

// var graph = require('./graph-db.js');

var express = require("express");
var app = express();

app.set("view engine", "pug");
app.set("views", __dirname + "/www_jade/");

// simple console logging of all requests
app.use(function(req, res, next){
	console.log('%s %s %s', req.ip, req.method, req.url);
	next();
});

var bodyParser = require("body-parser");
app.use(bodyParser.json({ type: 'application/*+json' }));
app.use(bodyParser.urlencoded({ type: 'application/x-www-form-urlencoded' }));

// static content routing
app.use(express.static(__dirname + "/www_static"));


// root redirects to portfolio
app.get("/", function(req, res, next){
	res.redirect("/portfolio/tags/featured")
});

app.get("/portfolio", function(req, res, next){
	res.redirect("/portfolio/tags/featured")
});

app.get("/tada", (req, res) => {
	res.redirect("/portfolio/projects/tada")
});

app.get("/switch", (req, res) => {
	res.redirect("/portfolio/tags/switch")
});

app.get("/mices", (req, res) => {
	res.redirect("/portfolio/tags/mices")
});

app.get("/ftf", (req, res) => {
	res.redirect("portfolio/projects/frank_the_flea")
});

// portfolio
require("./controllers/portfolio.js")(app);

// hallway computer
require("./controllers/hallwayComputer.js")(app);

// generic database actions
// require("./controllers/db.js")(app);

// TransMap
require("./controllers/transMap.js")(app);

// Email and SMS
require("./controllers/smsAndEmail.js")(app);

// dizziness
require("./controllers/dizziness.js")(app);


app.get(/graph/, (req, res, next) => {
	graph.cypher(req.query['q'])
		.then(
			result => {
				res.send(result)
			},
			next
		)
})


// GET /*/ ==> ./www_jade/*/index.jade
app.get(/^\/(.*)\/$/, function(req, res, next){
	res.render(req.params[0] + ".jade");
});

// GET /* ==> ./www_jade/*.jade
app.get(/^\/(.*)$/, function(req, res, next){
	res.render(req.params[0] + ".jade");
});

// error fallback ==> /www_jade/error.jade

app.use(function(err, req, res, next) {
	console.error("!!!\r" + err.message + "\r\r");

	var httpCode = err.message.startsWith("Failed to lookup") ? 404 : 500;
	if (err.code && (err.code >= 100 && err.code <= 999))
		httpCode = err.code;
	
	res.status(httpCode)
		.render("error.jade", {httpCode: httpCode, err: err});
});

// app.listen(8000);

var httpServer = http.createServer(app);
var httpsServer = https.createServer(credentials, app);

httpServer.listen(8000);
httpsServer.listen(8443);

console.log("Server running on port 8000 and 8443");
