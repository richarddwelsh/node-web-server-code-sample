let connectionStringLocation = (process.env.HOME || process.env.HOMEPATH ||
	process.env.USERPROFILE) + '/.credentials/mongoConnectionString.txt';
	
var Promise = require('bluebird'),
	fs = Promise.promisifyAll(require('fs'));

var MongoClient = require('mongodb').MongoClient,
	ObjectID = require('mongodb').ObjectID;

// Connection URL
var db;

// Use connect method to connect to the server
fs.readFileAsync(connectionStringLocation, 'utf8')
	.then(
		url => {
			MongoClient.connect(url)
			.then(
				x => {
					console.log("Connected successfully to 'test' database");
					db = x;
				}
			)
		}
	);

// query a collection
// http://mongodb.github.io/node-mongodb-native/2.2/api/Collection.html#find
exports.query = function(collName, query) {
	return db
		.collection(collName) // collection
		.find(query) // cursor
		.toArray(); // promise
}

// aggregation query
// http://mongodb.github.io/node-mongodb-native/2.2/api/Collection.html#aggregate
exports.aggregate = function(collName, pipeline) {
	return db
		.collection(collName) // collection
		// .find(query) // cursor
		.aggregate(pipeline) // cursor
		.toArray(); // promise
}

// find a document in a collection by id
// http://mongodb.github.io/node-mongodb-native/2.2/api/Collection.html#findOne
exports.findById = function(collName, id) {
	return db
		.collection(collName) // collection
		.findOne({'_id': new ObjectID(id)}); // promise
}

// insert doc into collection
// http://mongodb.github.io/node-mongodb-native/2.2/api/Collection.html#insertOne
exports.insert = function(collName, document) {
	return db
		.collection(collName) // collection
		.insertOne(document); // promise
}

// updates a document in a collection addressed by id
// http://mongodb.github.io/node-mongodb-native/2.2/api/Collection.html#updateOne
exports.updateById = function(collName, id, update) {
	return db
		.collection(collName) // collection
		.updateOne({'_id': new ObjectID(id)}, update); // promise
}

// updates a document in a collection addressed by id
// http://mongodb.github.io/node-mongodb-native/2.2/api/Collection.html#updateOne
exports.update = function(collName, query, update) {
	return db
		.collection(collName) // collection
		.updateOne(query, update); // promise
}

// remove a document by id
// http://mongodb.github.io/node-mongodb-native/2.2/api/Collection.html#deleteOne
exports.removeById = function(collName, id) {
	return db
		.collection(collName) // collection
		.deleteOne({'_id': new ObjectID(id)}); // promise
}

// query a collection returning distinct values of specified "key" field
// http://mongodb.github.io/node-mongodb-native/2.2/api/Collection.html#distinct
exports.distinct = function(collName, key, query) {
	return db
		.collection(collName) // collection
		.distinct(key, query); // cursor
}



/*

Example use of $lookup to do a "join"

db.liaisons.aggregate([{$lookup: {from: "stations", localField: "from", foreignField: "crsCode", as: "fromStation"}}])

*/