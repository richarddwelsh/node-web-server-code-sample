1. remove cards:
```
$("#godot div").animate({"top": "130px", "opacity": "0"}, {complete: function() { $(this).parent().remove() }})
```
2. reflow masonry:
```
$("#canvas").masonry({transitionDuration: '0.5s'})
```