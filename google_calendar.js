var Promise = require('bluebird'),
	fs = Promise.promisifyAll(require('fs'));

var google = require('googleapis');
var googleAuth = require('google-auth-library');
var TOKEN_DIR = (process.env.HOME || process.env.HOMEPATH ||
    process.env.USERPROFILE) + '/.credentials/';
var TOKEN_PATH = TOKEN_DIR + 'calendar-api-quickstart.json';

exports.getCalendarItems = function (optionsOverride) {
	// auth
	var auth = new googleAuth();
	var oauth2Client = new auth.OAuth2(
		"711231590991-g2p7jl3rahh8olrp124s7elbugr73dk1.apps.googleusercontent.com", // client_id
		"3OcSeeHBntXjrz7svK8mXsm-", // client_secret,
		"urn:ietf:wg:oauth:2.0:oob","http://localhost" // redirect_url
	);

	return fs.readFileAsync(TOKEN_PATH)
		.then(
			token => {
				// augment oauth2Client with contents of token
				oauth2Client.credentials = JSON.parse(token);
				
				// do API call here
				var calendar = google.calendar('v3');

				// default options
				var options = {
					auth: oauth2Client,
					calendarId: "primary",
					timeMin: (new Date()).toISOString(),
					maxResults: 500,
					singleEvents: true,
					orderBy: 'startTime'
				};

				Object.assign(options, optionsOverride)

				// Google API doesn't do promises :-(
				return new Promise(
					(resolve, reject) => {
						calendar.events.list(
							options,
							function(err, response) {
								if (err) 
									reject(err);
								else
									resolve(response);			
							}
						);						
					}
				)		
			}
		)
  }

  exports.getCalendars = function () {
	// auth
	var auth = new googleAuth();
	var oauth2Client = new auth.OAuth2(
		"711231590991-g2p7jl3rahh8olrp124s7elbugr73dk1.apps.googleusercontent.com", // client_id
		"3OcSeeHBntXjrz7svK8mXsm-", // client_secret,
		"urn:ietf:wg:oauth:2.0:oob","http://localhost" // redirect_url
	);

	return fs.readFileAsync(TOKEN_PATH)
		.then(
			token => {
				// augment oauth2Client with contents of token
				oauth2Client.credentials = JSON.parse(token);
				
				// do API call here
				var calendar = google.calendar('v3');

				// Google API doesn't do promises :-(
				return new Promise(
					(resolve, reject) => {
						calendar.calendarList.list(
							{
								auth: oauth2Client
							},
							function(err, response) {
								if (err) 
									reject(err);
								else
									resolve(response);			
							}
						);						
					}
				)		
			}
		)
  }