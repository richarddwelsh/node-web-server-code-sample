"use strict";

var db = require("../db.js");
var pug = require("pug");

var quickHttp = require('../http-get.js')
var ipFinder = /\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/;
var botfinder = /bot|spider/i;

var email = require("./smsAndEmail.js");
var visitors = {};

const querystring = require('querystring');

// returns a promise
function getProject(projId) {
	return db.aggregate("projects", [
		// aggregation pipeline
		{ $match: { "id": projId } }, // first select required project
		{ $lookup: {
			from: "skills", // skills collection
			localField: "skills",
			foreignField: "skill",
			as: "skillObjs"
		} },
		{ $lookup: {
			from: "projects", // projects collection
			localField: "skills",
			foreignField: "skills",
			as: "projectsSameSkill"
		} },
		{ $lookup: {
			from: "tags", // tags collection
			localField: "tags",
			foreignField: "tag",
			as: "tagObjs"
		} },
		{ $lookup: {
			from: "projects", // projects collection
			localField: "tags",
			foreignField: "tags",
			as: "projectsSameTag"
		} },
	])
}

module.exports = function(app) {

	
//	app.get(/\/portfolio\/.*/, function (req, res, next) {
/*		if (visitors[req.ip]) {
			if (!visitors[req.ip].paths.includes(req.url))
				visitors[req.ip].paths.push(req.url);
			visitors[req.ip].lastTime = new Date();
		}
		else
		{
			visitors[req.ip] = {
				paths: [req.url],
				lastTime: new Date(),
				userAgent: req.headers['user-agent'],
				protocol: req.protocol,
				hostname: req.hostname,
				referrer: req.headers['referer']
			}

			db.query("blacklist", {type: "ip", subject: req.ip})
				.then(
					result => {
						if (result.length == 0) { // IP not blacklisted

							if (req.headers['user-agent'] && !botfinder.exec(req.headers['user-agent'])) {

								let ipMatch = ipFinder.exec(req.ip);
								
								if (ipMatch) {
									quickHttp.httpGet("http://ip-api.com/json/" + ipMatch[0])
									.then(
										response => { 
											visitors[req.ip].lookup = response;
											if (response.status == "success") {
												email.sendMail({
													from: "Portfolio alert <dev@welshdesign.co.uk>",
													to: "rdwelsh@gmail.com",
													subject: "New visitor from " + response.org,
													text: `${req.headers['user-agent']}
Referrer: ${req.headers['referer']}
${response.as}, ${response.org}, ${response.city}, ${response.regionName}, ${response.country}

http://welshdesign.co.uk/portfolio/block_ip/${req.ip}

http://welsdesign.co.uk/portfolio/block_agent/${querystring.escape(req.headers['user-agent'])}`
												})							
											}
											else
											{
												email.sendMail({
													from: "Portfolio alert <dev@welshdesign.co.uk>",
													to: "rdwelsh@gmail.com",
													subject: "New visitor from " + ipMatch[0],
													text: `${req.headers['user-agent']}

http://welshdesign.co.uk/portfolio/block_ip/${req.ip}

http://welsdesign.co.uk/portfolio/block_agent/${querystring.escape(req.headers['user-agent'])}`
												})
											}
										}
									)
								}
								else
								{
									email.sendMail({
										from: "Portfolio alert <dev@welshdesign.co.uk>",
										to: "rdwelsh@gmail.com",
										subject: "New visitor from " + req.ip,
										text: `${req.headers['user-agent']}

http://welshdesign.co.uk/portfolio/block_ip/${req.ip}

http://welsdesign.co.uk/portfolio/block_agent/${querystring.escape(req.headers['user-agent'])}`
									})
								}
							}

						}
					},
					next
				)
		}
		next()
	})

	app.get(/\/portfolio\/block_ip\/([\d\.:a-f]+)/, (req, res, next) => {
		db.insert("blacklist", {type: "ip", subject: req.params[0], timestamp: new Date()})
			.then(
				dbResponse => res.send(`IP blocked: ${req.params[0]}`),
				next
			)
	})

	app.get(/\/portfolio\/block_agent\/(.+)/, (req, res, next) => {
		db.insert("blacklist", {type: "agent", subject: req.params[0], timestamp: new Date()})
			.then(
				dbResponse => res.send(`Agent blocked: ${req.params[0]}`),
				next
			)
	})
	
	app.get("/portfolio/visitors", function (req, res, next) {
		res.send(visitors);
	})

	*/

	// in use
	app.get(/\/portfolio\/(tags|skills|id)\/([a-zA-Z0-9_]+)/, function(req, res, next) {
		var allQueries = Promise.all([
			db.aggregate("projects", [
				// aggregation pipeline
				{ $match: { [req.params[0]]: {$in: [req.params[1]]} } }, // selection
				{ $lookup: { //join skills
					from: "skills", // skills collection
					localField: "skills",
					foreignField: "skill",
					as: "skillObjs"
				} }
			]), // data[0]
			db.query("skills"), // data[1]
			db.query("tags") // data[2]
		]);
	
		allQueries
			.then(
				data => res.render(
					"portfolioHome", {
						projects: data[0], 
						// confused? see arrayOfObjectsToAssociativeArray.md
						skills: Object.assign(...data[1].map(d => ({[d.skill]: d}))), 
						tags: Object.assign(...data[2].map(d => ({[d.tag]: d}))), 
						// filterBy: req.params[0],
						// filterById: req.params[1]
						context: {type: req.params[0], id: req.params[1]},
						pugRenderer: pug.render
					}
				),
				next
			)
	})
	
	app.get("/portfolio/tags/", function(req, res, next) {
		db.distinct("projects", "tags", null)
			.then(
				data => res.render("portfolioTagIndex.jade", {data: data}),
				next
			)
	})
	
	app.get("/portfolio/skills/", function(req, res, next) {
		db.distinct("projects", "skills", null)
			.then(
				data => res.render("portfolioSkillIndex.jade", {data: data}),
				next
			)
	})
	
	// in use
	app.get("/portfolio/tagMap", function(req, res, next) {
		db.aggregate("projects", [
				// "flatten" tags array in each project
				{ $unwind: "$tags" }, 
				// pivot project list to tag list, returning array of project ids for each tag 
				{ $group: { _id: "$tags", projects: { $push: "$id" } } 
				}
			])
			.then(
				data => res.send( Object.assign(...data.map(d => ({[d._id]: d})) ) ),
				next
			)
	})
	
	// in use
	app.get("/portfolio/skillMap", function(req, res, next) {
		db.aggregate("projects", [
				// "flatten" skills array in each project
				{ $unwind: "$skills" }, 
				// pivot project list to tag list, returning array of project ids for each skill 
				{ $group: { _id: "$skills", projects: { $push: "$id" } } 
				}
			])
			.then(
				data => res.send( Object.assign(...data.map(d => ({[d._id]: d})) ) ),
				next
			)
	})
	
	// in use
	app.get("/portfolio/projectData", function(req, res, next) {
		db.query("projects")
			.then(
				data => res.send( Object.assign(...data.map(d => ({[d.id]: d})) ) ),
				next
			)
	})

	app.get("/portfolio/skillData", function(req, res, next) {
		db.query("skills")
			.then(
				data => res.send( Object.assign(...data.map(d => ({[d.skill]: d})) ) ),
				next
			)
	})

	app.get("/portfolio/tagData", function(req, res, next) {
		db.query("tags")
			.then(
				data => res.send( Object.assign(...data.map(d => ({[d.tag]: d})) ) ),
				next
			)
	})
	
	app.get("/portfolio/skills", function(req, res, next) {
		db.distinct("projects", "skills", null)
			.then(
				data => res.render("portfolioSkillIndex.jade", {data: data}),
				next
			)
	})
	
	
	// app.get(/\/portfolio\/tags\/([a-zA-Z0-9_]+)/, function(req, res, next) {
	// 	db.query(
	// 			"projects"
	// 			// {"tags": {$in: [req.params[0]]} }
	// 		)
	// 		.then(
	// 			data => res.render("portfolioAll.jade", {data: data, tag: req.params[0]}),
	// 			next
	// 		)
	// })
	
	app.get("/portfolio/projects", function(req, res, next) {
		db.query(
				"projects",
				{}
			)
			.then(
				data => res.render("portfolioProjectIndex.jade", {data: data}),
				next
			)
	})
	
	// in use
	app.get(/\/portfolio\/projects\/([a-zA-Z0-9_]+)\/?$/, function(req, res, next) {
		getProject(req.params[0])
		.then(
			data => res.render("portfolioProject", {
				project: data[0],
				pugRenderer: pug.render
			}),
			next
		)
	})
	
	app.get(/\/portfolio\/projects\/([a-zA-Z0-9_]+)\/json/, function(req, res, next) {
		getProject(req.params[0])
		.then(
			data => res.json({
				project: data[0]
			}),
			next
		)
	})
	
	app.get(/\/portfolio\/projects\/([a-zA-Z0-9_]+)\/skillMatrix/, function(req, res, next) {
		getProject(req.params[0])
		.then(
			data => res.render("skillMatrix.pug", {project: data[0]}),
			next
		)
	})
	
	// in use
	app.get(/\/portfolio\/projects\/([a-zA-Z0-9_]+)\/tile/, function(req, res, next) {
		getProject(req.params[0])
		.then(
			data => res.render("projectTile.pug", {
				project: data[0],
				pugRenderer: pug.render
			}),
			next
		)
	})
	
	app.get("/portfolio/projects/skillMatrix", function(req, res, next) {
		var allQueries = Promise.all([
			db.query("projects"), // data[0]
			db.query("skills") //data[1]
		]);
	
		allQueries
		.then(
			data => res.render("skillMatrix.pug", {allProjects: data[0], allSkills: data[1]}),
			next
		)
	})
	
	app.get(/\/portfolio\/projects\/([a-zA-Z0-9_]+)\/thumbnailEditor/, function(req, res, next) {
		db.query("projects")
			.then(
				data => res.render("thumbnailEditor.jade", {
					projects: data,
					id: req.params[0]
				}),
				next
			)
	})

	app.post(/\/portfolio\/projects\/([a-zA-Z0-9_]+)\/updateThumbnail/, function(req, res, next){
		db.update(
				"projects", 
				{ "id": req.params[0] },
				{
					$set: {
						thumb: {
							xOffset: req.body.xOffset,
							yOffset: req.body.yOffset,
							zoomFactor: req.body.zoomFactor,
							path: req.body.path
						}
					}
				}
			)
			.then(
				result => res.status(200).type("json").send({updateCount: result})
			);
		});
	
}

const removeAfterDelay = 1800000 // half an hour in ms

setInterval(function() {
	for (var ip in visitors) {
		if ((new Date() - visitors[ip].lastTime) > removeAfterDelay)
			delete visitors[ip];
	}
}, removeAfterDelay)
