var db = require("../db.js");
var csv = require('csv-express');
var flatteners = require('../data/flatteners.js');

module.exports = function(app) {

	// Get /db/* ==> return JSON
	app.get(/\/db\/([a-zA-Z]+)\/?$/, function(req, res, next){
		db.query(
				req.params[0], // collection name
				req.query.q === undefined ? null : JSON.parse(req.query.q) // query, if specified in 'q' query parameter
			)
			.then(
				data => res
					.set("Access-Control-Allow-Origin", "*")
					.type("json")
					.status(200)
					.send(data.map( d => {
					if (d["_id"])
						d["_id"] = { "$oid": d["_id"] };
					return d;
				})),
				next
			)
	});


	// GET /db/*/csv ==> return [flattened] CSV
	app.get(/\/db\/([a-zA-Z]+)\/csv\/?$/, function(req, res, next){
		db.query(
				req.params[0], // collection name
				req.query.q === undefined ? null : JSON.parse(req.query.q) // query, if specified in 'q' query parameter
			)
			.then(
				data => {
					var flatData = flatteners[req.params[0]] ? data.map(flatteners[req.params[0]]) : data;
					res
						.set("Access-Control-Allow-Origin", "*")
						.status(200)
						.csv(flatData, true, {"Content-Type": "text/plain"})
				},
				next
			)
	});


	// GET /db/a-b/csv ==> return a-b relationship as CSV
	app.get(/\/db\/([a-zA-Z]+)\-([a-zA-Z]+)\/csv\/?$/, function(req, res, next) {
		db.aggregate(req.params[0], // collection name
			[
				// "flatten" nomnated array in each record
				{ $unwind: "$" + req.params[1] }
			])
			.then(
				data => { 
					var flatData = data.map( d => ({ id: d.id, fk: d[req.params[1]]}) );
					res
						.set("Access-Control-Allow-Origin", "*")
						.status(200)
						.csv(flatData, true, {"Content-Type": "text/plain"})
				},
				next
			)
	})
}