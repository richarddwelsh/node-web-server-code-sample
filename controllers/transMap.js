var db = require("../db.js");

// see www_jade/TransMap and www_static/TransMap

module.exports = function(app) {

	app.post("/TransMap/AddLiaison", function(req, res, next){
		// check first that the liaison doesn't already exist
		db.query(
				"liaisons",
				{  $or: 
					[
						{from: req.body.from.crsCode, to: req.body.to.crsCode},
						{from: req.body.to.crsCode, to: req.body.from.crsCode}
					]
				}	
			)
			.then(
				result => {
					if (result.length > 0)
						throw new Error("Liaison already exists");
					
					return db.insert(
						"liaisons",
						{ from: req.body.from.crsCode, to: req.body.to.crsCode }
					);
				}
			)
			.then(
				result => res.type("json").send(
					[{from: req.body.from.crsCode, to: req.body.to.crsCode}]
					),
				next
			)
		});
	
	
	
	app.post("/TransMap/RemoveLiaison", function(req, res, next){
		db.removeById(
				"liaisons", 
				req.body._id
			)
			.then(
				result => 
					res.status(200).type("json").send({removedCount: result})
			);
	});	
		
}