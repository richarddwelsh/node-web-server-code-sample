var Promise = require('bluebird'),
	fs = Promise.promisifyAll(require('fs'));

var nodemailer = require("nodemailer");

var PASSWD_DIR = (process.env.HOME || process.env.HOMEPATH ||
    process.env.USERPROFILE) + '/.credentials/';
var PASSWD_PATH = PASSWD_DIR + 'smtp_credentials.json';

// let transporter = nodemailer.createTransport({
//     host: 'mail.authsmtp.com',
//     port: 2525,
//     secure: false, // secure:true for port 465, secure:false for port 587
//     auth: {
//         user: 'ac27685',
//         pass: 'you-wish'
//     }
// });

console.log(PASSWD_PATH);

var transporter;
fs.readFileAsync(PASSWD_PATH)
	.then(
		json => transporter = nodemailer.createTransport(
			JSON.parse(json)
		),
	err => console.log(err)
	)

module.exports = function(app) {

	app.get("/nexmo_in", function(req, res, next){
		var augmented_record = req.query;
		augmented_record.unread = true;
		augmented_record.timestamp = new Date(req.query["message-timestamp"]);
	
		var emailOut = {
			from: "Text forwarding <dev@welshdesign.co.uk>",
			to: "rdwelsh@gmail.com",
			subject: "Forwarded text from " + augmented_record.msisdn,
			text: augmented_record.text
		}
	
		transporter.sendMail(emailOut)
			.then(
				result => res.type("json").send(result),
				next
			)
	})

	app.post("/Testrigs/mailout", function(req, res, next){
		var emailOut = {
			from: "Dev <dev@welshdesign.co.uk>",
			to: req.body.to,
			subject: req.body.subject,
			html: req.body.body_html
		}

		transporter.sendMail(emailOut)
			.then(
				response => {
					var data = req.body;
					data.response = response;
					res.render("Testrigs/mailout.jade", {postedData: data})}
					,
				next
			)
		})
	
}

module.exports.sendMail = function(emailOut) {
	transporter.sendMail(emailOut);
}
