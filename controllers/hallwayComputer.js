var db = require("../db.js");
var quickHttp = require("../http-get"); // one of mine
var tz = require("timezone"),
	ukTime = tz(require("timezone/en_GB"), require("timezone/Europe/London"));
var googleCal = require("../google_calendar.js");
// xpathSelect = requireVerbose("./xpath-select");

module.exports = function(app) {

	app.get("/scraper", function(req, res, next) {
		var url = req.query.url || "http://www.bbc.co.uk/weather/ox2",
			path = req.query.path;
	
		quickHttp.httpGet(url)
			.then(
				response => res.send(response),
				next
			)
	})

	app.get("/google_calendar", function(req, res, next){
		googleCal.getCalendarItems()
			.then(
				data => res.render("googleCal.jade", {data: data, ukTime, ukTime}),
				next
			)
	})

	app.get("/google_calendars", function(req, res, next){
		googleCal.getCalendars()
			.then(
				data => res.send(data),
				next
			)
	})

	app.get(/\/google_calendar\/([^\/]+)/, function(req, res, next){
		googleCal.getCalendarItems({calendarId: req.params[0]})
			.then(
				data => res.send(data),
				next
			)
	})

	app.get("/unread_sms", function(req, res, next){
		db.query(
				"sms",
				{timestamp: {$gt: new Date(new Date().setDate(new Date().getDate()-7))}}
			)
			.then(
				data => res.render("smsList.jade", {data: data, ukTime: ukTime}),
				next
			)
	})

	// this is for the smartphone SMS page
	app.get("/ack_sms", function(req, res, next){
		db.query(
				"sms",
				{unread: true}
			)
			.then(
				data => res.render("smsAck.jade", {data: data, ukTime: ukTime}),
				next
			)
	})

	app.get("/mark_as_read", function(req, res, next) {
		db.findById("sms", req.query.id)
			.then(
				originalMessage => {
					if (originalMessage == null)
						throw new Error("Message not found");
	
					return quickHttp.httpGet(
						"https://rest.nexmo.com/sms/json",
						{
							api_key: "fd84a9c7",
							api_secret: "52891f5b",
							from: "447937947994",
							to: originalMessage.msisdn,
							// TODO: maybe truncate so that always fits in one message?
							text: "Richard has seen your message: \"" + originalMessage.text + "\""	
						}
					);
				}
			)
			.then(
				response => 
					db.updateById("sms", req.query.id, {unread: false}),
				next
			)
			.then(
				data => res.type("json").send({messagesMarked: data}),
				next
			)
	})

}