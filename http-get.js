// var http = require('http');
// 	//parse5 = require('parse5'),
// 	//parser = new parse5.Parser();

// exports.httpGet = function(url, callback) {
// 	var req = http.get(url, function(resp){
// 		console.log("GET started", url, ":", resp.statusCode);

// 		var bodyText = "";

// 		resp.setEncoding("utf8"); // pretty safe guess these days

// 		resp.on("data", function(chunk) {
// 			//console.log(chunk.length);
// 			bodyText += chunk;
// 		})

// 		resp.on("end", function(){
// 			console.log("GET fininshed", url, bodyText.length);
// 			//var doc = parser.parse(bodyText);
// 			callback(null, bodyText)	
// 		})
// 	})

// 	req.on("error", function(e){
// 		callback(e)
// 	})
// }


// see http://unirest.io/nodejs.html

var unirest = require('unirest');

// exports.httpGet = function(url, callback) {
// 	unirest.get(url)
// 		.end(function(response){
// 			if (response.error)
// 				callback(response.error)
// 			else
// 				callback(null, response.body)
// 		})
// }

// now as a promise
exports.httpGet = function(url, query) {
	query = query || {};

	return new Promise(
		(resolve, reject) => {
			unirest.get(url)
				.query(query)
				.end(
					response => {
						if (response.error)
							reject(response.error)
						else
							resolve(response.body)
					}
				)
			}
		)
}